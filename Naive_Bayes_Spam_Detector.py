#Naive Bayes Spam Detector
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline #Pipeline komutu
from sklearn.naive_bayes import MultinomialNB #Bayes modülü
from sklearn.feature_extraction.text import CountVectorizer #Bu ise Text'i nümerik hale çevirir (Multiple Halde)

df = pd.read_csv(r'C:\Users\Asus\Desktop\DATA\spam.csv')
# print(df)
# print(df.groupby('Category').describe())

df['spam']=df['Category'].apply(lambda x: 1 if x=='spam' else 0)  #Metinsel ifadeyi 0 ve 1 olarak çevirdik #Spam kutusu

X_train, X_test, y_train, y_test = train_test_split(df.Message,df.spam)

#Spamları 0 ve 1 olarak alabilsek de "Message" kısımı daha komplike olduğu için
#Vectorizer'dan yararlanıyoruz

clf = Pipeline([
    ('vectorizer', CountVectorizer()), ## Vektörleştirici burada
    ('nb', MultinomialNB()) #Burada Bayes modülünün hangisi olduğu 
])

clf.fit(X_train, y_train)

#Örnek:
emails = [
    'Hey mohan, can we get together to watch footbal game tomorrow?',
    'Upto 20% discount on parking, exclusive offer just for you. Dont miss this reward!',
    'There is something here',
    'Dont miss this reward!',
]

print(clf.predict(emails))
print(clf.score(X_test , y_test))


# if clf.predict == 0:
#     print("Bu bir spamdır")
# else:
#     print("Bu bir Spam Değildir")